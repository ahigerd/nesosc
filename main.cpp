#include "filter.h"
#include "oscillator.h"
#include "riffwriter.h"
#include <vector>
#include <iostream>

int main(int argc, char** argv) {
  std::vector<int16_t> wave;

  SquareOscillator square;
  square.frequency = 110;
  FamicomFilter ff_square(&square);

  TriangleOscillator tri;
  tri.frequency = 110;
  FamicomFilter ff_tri(&tri);

  NoiseOscillator noise;
  noise.frequency = 880;
  FamicomFilter ff_noise(&noise);

  for (int i = 0; i < 48000; i++) {
    wave.push_back(ff_square.getSample(i / 48000.0));
  }

  for (int i = 0; i < 48000; i++) {
    wave.push_back(ff_tri.getSample(i / 48000.0));
  }

  for (int i = 0; i < 48000; i++) {
    wave.push_back(ff_noise.getSample(i / 48000.0));
  }

  for (int seed = 1; seed < 16; seed++) {
    Noise93Oscillator noise93((1 << seed) | 1);
    noise93.frequency = 1760 * 2;
    FamicomFilter ff_noise93(&noise93);
    for (int i = 0; i < 6000; i++) {
      wave.push_back(0);
    }
    for (int i = 0; i < 12000; i++) {
      wave.push_back(ff_noise93.getSample(i / 48000.0));
    }
  }

  RiffWriter riff(48000, false, wave.size() * 2);
  if (argc > 1) {
    riff.open(argv[1]);
  } else {
    riff.open("nesosc.wav");
  }
  riff.write(wave);
  riff.close();
  return 0;
}
