all: nesosc

nesosc: $(patsubst %.cpp, %.o, $(wildcard *.cpp))
	g++ -std=gnu++14 -o $@ $^

%.o: %.cpp $(wildcard *.h) Makefile
	g++ -std=gnu++14 -ggdb -c -o $@ $<
