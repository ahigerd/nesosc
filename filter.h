#ifndef FILTER_H
#define FILTER_H

#include "audionode.h"

class FamicomFilter : public AudioNode {
public:
  FamicomFilter(AudioNode* source, double decayRate = .15, double lagRate = .00004);

  virtual int16_t getSample(double time);

private:
  AudioNode* source;
  double decayRate;
  double lagRate;
  int16_t lastSample;
  double decayTime;
  double lastOutput;
  double lastTime;
};

#endif
