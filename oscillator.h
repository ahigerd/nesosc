#ifndef OSCILLATOR_H
#define OSCILLATOR_H

#include "audionode.h"

class BaseOscillator : public AudioNode {
public:
  double frequency;
  double amplitude;

  virtual int16_t getSample(double time);

protected:
  BaseOscillator();

  int16_t lastSample;
  double lastTime;
  double phase;

  virtual double calcSample(double time) = 0;
};

class SquareOscillator : public BaseOscillator {
public:
  SquareOscillator(double duty = 0.5);

  double dutyCycle;

protected:
  virtual double calcSample(double time);
};

class TriangleOscillator : public BaseOscillator {
public:
  TriangleOscillator(int quantize = 16, bool skew = true);

  int quantize;
  bool skew;

protected:
  virtual double calcSample(double time);
};

class NoiseOscillator : public BaseOscillator {
public:
  NoiseOscillator();

protected:
  virtual double calcSample(double time);

private:
  uint16_t state;
  double lastPhase;
};

class Noise93Oscillator : public BaseOscillator {
public:
  Noise93Oscillator(uint16_t seed = 1);

protected:
  virtual double calcSample(double time);

private:
  uint16_t state;
  double lastPhase;
};

#endif
