#include "filter.h"
#include <cmath>
#include <iostream>

FamicomFilter::FamicomFilter(AudioNode* source, double decayRate, double lagRate)
: source(source), decayRate(decayRate), lagRate(lagRate), lastSample(0), decayTime(0), lastOutput(0), lastTime(0)
{
  // initializers only
}

int16_t FamicomFilter::getSample(double time)
{
  int16_t sample = source->getSample(time);
  if (sample != lastSample) {
    decayTime = time;
    lastSample = sample;
  }
  double fsample = sample / 32768.0;
  double decayStep = (time - decayTime) * 1000.0 * decayRate;
  // The output decays from the maximum value to a more steady value.
  // This formula is not fully accurate but on small time scales it's close enough.
  double decay = std::abs(decayStep * fsample);
  if (decay > .5) {
    decay = .25 * (fsample < 0 ? -1 : 1);
  } else {
    decay = decay * (1 - decay) * (fsample < 0 ? -1 : 1);
  }
  fsample -= decay;
  // The output also can't change instantaneously, so add some hysteresis.
  double timeStep = time == lastTime ? 1 : (time - lastTime) / lagRate;
  double outputStep = fsample - lastOutput;
  lastOutput += outputStep * timeStep;
  lastTime = time;
  return std::round(lastOutput * 32768.0);
}
