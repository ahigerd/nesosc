#include "oscillator.h"
#include <cmath>
#include <iostream>

BaseOscillator::BaseOscillator()
: frequency(440), amplitude(1.0), lastSample(0), lastTime(0), phase(0)
{
  // initializers only
}

int16_t BaseOscillator::getSample(double time)
{
  if (amplitude == 0.0) {
    lastSample = 0;
    return 0;
  }
  if (time == lastTime) {
    return lastSample;
  }
  lastSample = std::round(calcSample(time) * amplitude * 16384);
  double phaseOffset = (time - lastTime) * frequency;
  phase = std::fmod(phase + phaseOffset, 1.0);
  lastTime = time;
  return lastSample;
}

SquareOscillator::SquareOscillator(double duty)
: BaseOscillator(), dutyCycle(duty)
{
  // initializers only
}

double SquareOscillator::calcSample(double)
{
  return phase < dutyCycle ? 1.0 : -1.0;
}

TriangleOscillator::TriangleOscillator(int quantize, bool skew)
: BaseOscillator(), quantize(quantize), skew(skew)
{
  // initializers only
}

double TriangleOscillator::calcSample(double)
{
  double lphase = phase * 2.0;
  double level = lphase < 1.0 ? lphase : 2.0 - lphase;
  if (quantize > 1) {
    level = std::floor(level * quantize) / double(quantize - 1);
    if (skew) {
      // This isn't physically accurate to how the hardware actually generates this,
      // which has a nonlinearity that's somewhat difficult to model accurately.
      if (phase < 0.5) {
        level = 1.0 - level;
        level = (level + level * level) * .5;
        level = 1.0 - level;
      } else {
        level = (level + level * level) * .5;
      }
    }
  }
  return level * 2.0 - 1.0;
}

NoiseOscillator::NoiseOscillator()
: BaseOscillator(), state(1), lastPhase(0)
{
  // initializers only
}

double NoiseOscillator::calcSample(double)
{
  if (lastPhase > phase) {
    bool bit = !(state & 0x0001) != !(state & 0x0002);
    state = (state >> 1) | (bit ? 0x4000 : 0);
  }
  lastPhase = phase;
  return (state & 0x0001) ? 1 : -1;
}

Noise93Oscillator::Noise93Oscillator(uint16_t seed)
: BaseOscillator(), state(seed), lastPhase(0)
{
  // initializers only
}

double Noise93Oscillator::calcSample(double)
{
  if (lastPhase > phase) {
    bool bit = !(state & 0x0001) != !(state & 0x0040);
    state = (state >> 1) | (bit ? 0x4000 : 0);
  }
  lastPhase = phase;
  return (state & 0x0001) ? 1 : -1;
}

