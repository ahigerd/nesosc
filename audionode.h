#ifndef AUDIONODE_H
#define AUDIONODE_H

#include <cstdint>

class AudioNode {
public:
  virtual int16_t getSample(double time) = 0;
};

#endif
